package UD20Ejercicio5.UD20Ejercicio5;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.SpringLayout;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Ejercicio5App extends JFrame {

	private JPanel contentPane;
	private JTextArea textArea;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio5App frame = new Ejercicio5App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejercicio5App() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				textArea.setText(textArea.getText() + " Click ");
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				textArea.setText(textArea.getText() + " Entered ");
			}
			@Override
			public void mouseExited(MouseEvent e) {
				textArea.setText(textArea.getText() + " Exited ");
			}
			@Override
			public void mousePressed(MouseEvent e) {
				textArea.setText(textArea.getText() + " Pressed ");
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				textArea.setText(textArea.getText() + " Released ");
			}
		});
		sl_contentPane.putConstraint(SpringLayout.NORTH, textArea, -195, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, textArea, 10, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, textArea, -10, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, textArea, -8, SpringLayout.EAST, contentPane);
		contentPane.add(textArea);
		
		btnNewButton = new JButton("Limpiar");
		sl_contentPane.putConstraint(SpringLayout.NORTH, btnNewButton, 10, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, btnNewButton, 148, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btnNewButton, -11, SpringLayout.NORTH, textArea);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnNewButton, -143, SpringLayout.EAST, contentPane);
		contentPane.add(btnNewButton);
		
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiar();
			}
		});
	}
	
	public void limpiar() {
		textArea.setText("");
	}
}
